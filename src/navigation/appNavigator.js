import {createStackNavigator,} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';

import Index from '../screens/container/tabbar';

console.disableYellowBox = true;

const AppNavigator = createStackNavigator(
    {
        Index
    },
    {
        initialRouteName: 'Index',
        initialRouteParams: 'Splash',
        headerMode: 'none',
        // transitionConfig: (Constant.isIOS) && NewTransitionConfiguration || AndroidTransitionConfiguration,
    },
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
