import React, {Component} from 'react';
import {StatusBar, View, AsyncStorage} from 'react-native';
import MyApp from '../navigation/appNavigator';

export default class App extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <MyApp/>
        );
    }
}
