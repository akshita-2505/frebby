import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Image,
    ScrollView,
    FlatList
} from 'react-native';
import {color, wp, hp, fontSize, boxShadow} from '../../../../helper/appHelper';
import {nearbyOffers, nearbyMarchents} from '../../../../helper/constant';

export default class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderItem = (item, index) => {
        let image = item.item.image;
        return(
            <View style={{marginRight: 10, height: wp(50), width: wp(50)}}>
                <Image source={{uri: image}}
                       style={{height: '100%', width: '100%'}}
                       resizeMode={'contain'}/>
            </View>
        )
    };

    renderItemMerchents = (item, index) => {
        let image = item.item.image;
        return(
            <View style={{marginRight: 10}}>
                <Image source={{uri: image}}
                       style={{height: wp(30), width: wp(43)}}
                       resizeMode={'contain'}/>
            </View>
        )
    };

    render() {
        const { container, common, header, headerText } = styles;
        return (
            <View style={[container]}>
                <View style={header}>
                    <Text style={headerText}>{'Home'}</Text>
                    <Image source={{uri: 'user_profile'}}
                           style={{height: hp(5),
                               width: hp(5)}} resizeMode={'contain'}/>
                </View>
                <View style={[boxShadow, {marginTop: 17, backgroundColor: 'white',
                    paddingHorizontal: '5%', paddingVertical: '5%', borderRadius: 10}]}>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{fontWeight: 'bold'}}>{'1200 '}</Text>
                        <Text style={{color: color.gray}}>{'USD'}</Text>
                    </View>
                    <Text style={{color: '#9E00A7'}}>{'+1k pointes'}</Text>
                    <View style={{
                        paddingTop: '10%',
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                    }}>
                        <Text style={{color: color.blue}}>{'Awesome Fekry'}</Text>
                        <Image source={{uri: 'golden_user'}} style={{height: '50%',
                            width: '40%'}} resizeMode={'contain'}/>
                    </View>
                </View>
                <View style={{marginVertical: hp(2.5)}}>
                    <Text style={{color: color.gray}}>{'Nearby Offers'}</Text>
                    <FlatList
                        data={nearbyOffers}
                        horizontal
                        renderItem={this.renderItem}
                        keyExtractor={item => item.id}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
                <View style={{marginTop: -5}}>
                    <Text style={{color: color.gray}}>{'Nearby Merchents'}</Text>
                    <FlatList
                        data={nearbyMarchents}
                        renderItem={this.renderItemMerchents}
                        keyExtractor={item => item.id}
                        numColumns={2}
                        style={{top: 10}}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    common: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        flex: 1,
        // backgroundColor: 'yellow',
        borderBottomWidth: 0.5,
        borderColor: color.gray,
        paddingHorizontal: wp(5)
    },
    header: {
        marginTop: hp(2),
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    headerText: {
        fontSize: fontSize.f30,
        fontWeight: 'bold'
    }

})
