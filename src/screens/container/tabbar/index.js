import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    AppState,
    SafeAreaView
} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {EventRegister} from 'react-native-event-listeners';
import Home from './homeTab';
import Notify from './notifyTab';
import Menu from './menuTab';
import TabbarIcon from '../../common/tabIcon';

const RootTabs = createBottomTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            tabBarIcon: ({ tintColor, focused }) => {
                return (
                    <TabbarIcon
                        tintColor={tintColor}
                        tabbar={"Home"}
                        focused={focused}
                    />
                )
            },
            tabBarOnPress: ({navigation, defaultHandler}) =>{
                EventRegister.emit('tabChangeListner', "home");
                return defaultHandler()

            }
        },
    },
    Notify: {
        screen: Notify,
        navigationOptions: {
            tabBarIcon: ({ tintColor, focused }) => {
                return (
                    <TabbarIcon
                        tintColor={tintColor}
                        tabbar={"Notify"}
                        focused={focused}
                    />
                )
            },
            tabBarOnPress: ({navigation, defaultHandler}) =>{
                EventRegister.emit('tabChangeListner', "notify");
                return defaultHandler()
            }
        },
    },
    Menu: {
        screen: Menu,
        navigationOptions: {
            tabBarIcon: ({ tintColor, focused }) => {
                return (
                    <TabbarIcon
                        tintColor={tintColor}
                        tabbar={"Menu"}
                        focused={focused}
                    />
                )
            },
            tabBarOnPress: ({navigation, defaultHandler}) =>{
                EventRegister.emit('tabChangeListner', "menu");
                return defaultHandler()
            }
        },
    }
},{
    tabBarOptions:{
        showLabel: false,
        style:{
            backgroundColor: "transparent",
            borderTopWidth:0,
            height:50,
            // zIndex:6
        },
        tabStyle: {
            width: 100,
        },
    },
    lazy: false,
    swipeEnabled: false,
    animationEnabled: false,
    initialRouteName: 'Home'
});

export default class RootTabNavigation extends React.Component {

    static router = RootTabs.router;

    constructor(props) {
        super(props);
    }

    onTabChange = (selectedTab) => {
        if(selectedTab == "Home"){
        }
    };

    render() {
        return (
            <View style={[styles.container]}>
                <SafeAreaView/>
                <RootTabs ref={(c) => this._rootTabView = c}
                          navigation={this.props.navigation}/>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderTopWidth: 1,
        borderColor: 'red',
        backgroundColor: 'transparent',
        marginTop:0,
        overflow: 'hidden',
    }
});
