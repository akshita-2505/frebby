import React from 'react';
import { Image, View } from 'react-native';
import { boxShadow, color, screen, fontSize, hp } from '../../helper/appHelper';
import { selectedTabIcon, tabIcon } from '../../helper/constant';

export default class TabbarIcon extends React.Component {
    render() {
        const { tabbar, focused, tabbaractivate } = this.props;
        return (
            <View style={{
                zIndex: 0,
                width: screen.width/8,
                height: 50,
                justifyContent: 'center',
                alignSelf:'center',
                }}>
                {
                    (tabbar === 'Notify') &&
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={{uri: selectedTabIcon[tabbar]}}
                           style={{
                               height: hp(3),
                               width: hp(3),
                               alignSelf:'center',
                               zIndex: 11111
                           }}
                           resizeMode={'contain'}/>
                           <Image source={{uri: 'oval'}}
                                  style={{height: hp(6.3), width: hp(6.3), position: 'absolute'}}
                                  resizeMode={'contain'}/>
                            <Image source={{uri: 'oval'}}
                                   style={{height: hp(5.2), width: hp(5.2), position: 'absolute'}}
                                   resizeMode={'contain'}/>
                            <Image source={{uri: 'oval'}}
                                   style={{height: hp(4), width: hp(4), position: 'absolute', borderRadius: 17.5, backgroundColor: focused && color.blue || 'transparent'}}
                                   resizeMode={'contain'}/>
                        </View>
                    || <Image source={focused && {uri: selectedTabIcon[tabbar]} || {uri : tabIcon[this.props.tabbar]}}
                                                       style={{
                                                           height: hp(3.2),
                                                           width: hp(3.2),
                                                           alignSelf:'center',
                                                       }}
                                                       resizeMode={'contain'}/>
                }
                <View style={{left:-screen.width/16 ,top:0, width:screen.width/4, height:0.5 ,
                    position:'absolute',
                    shadowOffset: { width: 0, height: -3 },
                    shadowOpacity: 0.7,
                    shadowRadius: 4,
                    elevation: 1}}/>
            </View>
        );
    }
}
