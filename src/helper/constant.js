module.exports = {
    tabIcon: {
        Home: 'home_inactive_tab',
        Notify: 'noun_pay',
        Menu: 'more_inactive_tab',
    },
    selectedTabIcon: {
        Home: 'home_tab',
        Notify: 'noun_pay',
        Menu: 'more_tab',
    },
    nearbyOffers: [
        {
            image: 'mobile',
            title: '40% off an iPhone X',
            subtitle: 'Tradeline Stores'
        },
        {
            image: 'coffee',
            title: 'Free cup of Coffee',
            subtitle: 'Starbucks'
        }
    ],
    nearbyMarchents: [
        {
            image: 'merchent_1',
            title: '40% off an iPhone X',
            subtitle: 'Tradeline Stores'
        },
        {
            image: 'merchent_2',
            title: 'Free cup of Coffee',
            subtitle: 'Starbucks'
        },
        {
            image: 'merchent_1',
            title: '40% off an iPhone X',
            subtitle: 'Tradeline Stores'
        },
        {
            image: 'merchent_2',
            title: 'Free cup of Coffee',
            subtitle: 'Starbucks'
        }
    ]
};
