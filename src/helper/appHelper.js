import {Dimensions, PixelRatio, Platform} from 'react-native';

export const { height, width } = Dimensions.get('window');
const screenWidth = Dimensions.get('window').width;
const screenHeight =
    (Platform.OS === 'iOS' && Dimensions.get('window').height) ||
    Dimensions.get('window').height - 35;

export const screen = {
    screen: Dimensions.get('window'),
    height: Platform.OS === 'ios' && Dimensions.get('window').height || Dimensions.get('window').height - 24,
    width: Dimensions.get('window').width,
    fullScreenHeight: Dimensions.get('window').height,
};

export const constant = {
    isIOS: Platform.OS === 'ios',
    isiPAD: ((Dimensions.get('window').height/Dimensions.get('window').width) < 1.6),
    isANDROID: Platform.OS === 'android',

    transparent: 'transparent',

}

export const boxShadow = {
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    elevation: 1,
};

export const color = {
    'silver' : 'silver',
    'white' : 'white',
    'black' : 'black',
    'blue' : '#2711D8',
    'gray' : '#7F7F7F'
};

export const wp = wp => {
    const elemWidth =
        typeof wp === 'number' ? wp : parseFloat(wp);
    return PixelRatio.roundToNearestPixel((screenWidth * elemWidth) / 100);
};

export const hp = hp => {
    const elemHeight =
        typeof hp === 'number'
            ? hp
            : parseFloat(hp);
    return PixelRatio.roundToNearestPixel((screenHeight * elemHeight) / 100);
};

const scale = screenWidth / 375;

export function normalize(size) {
    const newSize = size * scale;
    if (Platform.OS === 'ios') {
        return Math.round(PixelRatio.roundToNearestPixel(newSize));
    } else {
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
    }
}

export const fontSize = {
    f10: normalize(10),
    f11: normalize(11),
    f12: normalize(12),
    f13: normalize(13),
    f14: normalize(14),
    f15: normalize(15),
    f16: normalize(16),
    f17: normalize(17),
    f18: normalize(18),
    f19: normalize(19),
    f20: normalize(20),
    f21: normalize(21),
    f25: normalize(25),
    f30: normalize(30),
};
